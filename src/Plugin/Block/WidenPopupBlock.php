<?php

namespace Drupal\widencollective_filefield\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'widencollective_filefield' block.
 *
 * @Block(
 *   id = "widen_filefield_popup",
 *   admin_label = @Translation("Widen Filefield Popup"),
 *   category = @Translation("Custom")
 * )
 */
class WidenPopupBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $widen_popup_content = '<div class="openwiden"></div><div class="widen-modal" id="widenModal"><div class="widen-modal-content"><div class="widen-modal-header"><span class="widen-modal-close">×</span></div><div class="widen-content">&nbsp;</div></div></div>';
    return [
      '#type' => 'markup',
      '#markup' => $widen_popup_content,
    ];
  }

}
