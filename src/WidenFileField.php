<?php

namespace Drupal\widencollective_filefield;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Defines methods for integrating Widen into file field widgets.
 */
class WidenFileField implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderWidget'];
  }

  /**
   * Returns a list of supported widgets.
   */
  public static function supportedWidgets() {
    $widgets = &drupal_static(__FUNCTION__);
    if (!isset($widgets)) {
      $widgets = ['file_generic', 'image_image'];
      $widgets = array_unique($widgets);
    }
    return $widgets;
  }

  /**
   * Checks if a widget is supported.
   */
  public static function isWidgetSupported(WidgetInterface $widget) {
    return in_array($widget->getPluginId(), static::supportedWidgets());
  }

  /**
   * Returns widget settings form.
   */
  public static function widgetSettingsForm(WidgetInterface $widget) {
    $form = [];
    if (static::isWidgetSupported($widget)) {
      $form['widen_enabled'] = [
        '#type' => 'checkbox',
        '#title' => t('Allow users to select files from Widen DAM'),
        '#default_value' => $widget->getThirdPartySetting('widencollective_filefield', 'widen_enabled'),
      ];
    }
    return $form;
  }

  /**
   * Alters the summary of widget settings form.
   */
  public static function alterWidgetSettingsSummary(&$summary, $context) {
    $widget = $context['widget'];
    if (static::isWidgetSupported($widget)) {
      $status = $widget->getThirdPartySetting('widencollective_filefield', 'widen_enabled') ? t('Yes') : t('No');
      $summary[] = t('Widen DAM enabled: @status', ['@status' => $status]);
    }
  }

  /**
   * Processes widget form.
   */
  public static function processWidget($element, FormStateInterface $form_state, $form) {
    $element['widen_paths'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['widen-filefield-paths'],
        'data-widen-url' => '/widen/public',
      ],
      '#value' => '',
    ];
    $element['#attached']['library'][] = 'widencollective_filefield/drupal.widen.filefield';
    $element['#pre_render'][] = [get_called_class(), 'preRenderWidget'];
    return $element;
  }

  /**
   * Pre-renders widget form.
   */
  public static function preRenderWidget($element) {
    if (!empty($element['#value']['fids'])) {
      $element['widen_paths']['#access'] = FALSE;
    }
    return $element;
  }

  /**
   * Sets widget file id values by validating and processing the submitted data.
   */
  public static function setWidgetValue($element, &$input, FormStateInterface $form_state) {
    if (empty($input['widen_paths'])) {
      return;
    }
    $external = $input['widen_paths'];
    $input['widen_paths'] = '';
    $file_usage = \Drupal::service('file.usage');
    $errors = [];
    if (!empty($external)) {
      $file = static::getFileEntity($external, TRUE);
      if ($new_errors = file_validate($file, $element['#upload_validators'])) {
        $errors = array_merge($errors, $new_errors);
      }
      else {
        if ($file->isNew()) {
          $file->save();
        }
        if ($fid = $file->id()) {
          if (!$file_usage->listUsage($file)) {
            $file_usage->add($file, 'widencollective_filefield', 'file', $fid);
          }
          $input['fids'][] = $fid;
        }
      }
    }
    if ($errors) {
      $errors = array_unique($errors);
      if (count($errors) > 1) {
        $errors = ['#theme' => 'item_list', '#items' => $errors];
        $message = \Drupal::service('renderer')->render($errors);
      }
      else {
        $message = array_pop($errors);
      }
      \Drupal::messenger()->addMessage($message, 'error');
    }
  }

  /**
   * Returns a managed file entity by uri.
   */
  public static function getFileEntity($uri, $create = FALSE, $save = FALSE) {
    $file = FALSE;
    if ($files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $uri])) {
      $file = reset($files);
    }
    elseif ($create) {
      $file = static::createFileEntity($uri, $save);
    }
    return $file;
  }

  /**
   * Creates a file entity with an uri.
   */
  public static function createFileEntity($uri, $save = FALSE) {
    $values = [
      'uri' => $uri,
      'uid' => \Drupal::currentUser()->id(),
      'status' => 1,
      'filesize' => filesize($uri),
      'filename' => \Drupal::service('file_system')->basename($uri),
      'filemime' => \Drupal::service('file.mime_type.guesser')->guess($uri),
    ];
    $file = \Drupal::entityTypeManager()->getStorage('file')->create($values);
    if ($save) {
      $file->save();
    }
    return $file;
  }

}
