# WIDEN COLLECTIVE FILE FIELD - DRUPAL MODULE

INTRODUCTION
------------

Widen develop software solutions for marketers who need to connect their visual
content – like graphics, logos, photos, videos, presentations and more –
for greater visibility and brand consistency.

This module allows your Drupal projects to connect to the API of the Digital
Asset Management system Widen Collective with the image and file fields.

REQUIREMENTS
------------

  This module requires widen collective module as its dependent module. See: 
  https://www.drupal.org/project/widencollective

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules
   for further information.

CONFIGURATION
------------

 * Enable 'Widen Filefield Popup' block in administration theme as below, 
  Steps:
   1. Go to Structure -> Block Layout -> Click Seven(default) theme.
   2. Click Place Block on the content region
   3. Select the 'Widen Filefield Popup' and click Place Block
   4. Uncheck Display Title
   5. Click on pages tab and add the below lines,
        /node/add/*
        /node/*/edit
        /block/add/*
        /block/*
        /taxonomy/term/*/edit
        /admin/structure/taxonomy/manage/*/add
      Add any URL's if needed.
      [Note: Make sure 'Show for the listed pages' is enabled]
   6. Click Save Block

IMAGE / FILE FIELD INTEGRATION
------------------------------

 1. Go to form settings of your content type.
    Ex: /admin/structure/types/manage/[any content type]/form-display
 2. Edit widget settings of a file/image field.
 3. Check the box saying "Allow users to select files from Widen DAM".
 4. Click Save.
 5. You should now see the "Open Widen Window" link above the upload widget 
    in the content form for the enabled fields. 
