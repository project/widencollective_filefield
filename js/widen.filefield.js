(function ($, Drupal) {
  "use strict";
  var widenFileField = window.widenFileField = {};
  Drupal.behaviors.widenFileField = {
    attach: function (context, settings) {
      var i;
      var el;
      var $els = $('.widen-filefield-paths', context).not('.iff-processed').addClass('iff-processed');
      for (i = 0; el = $els[i]; i++) {
        widenFileField.processInput(el);
      }
    }
  };
  widenFileField.processInput = function (el) {
    var widget;
    var url = el.getAttribute('data-widen-url');
    var fieldId = el.getAttribute('data-drupal-selector').split('-widen-paths')[0];
    if (url && fieldId) {
      url += (url.indexOf('?') === -1 ? '?' : '&') + 'sendto=widenFileField.sendto&fieldId=' + fieldId;
      widget = $(widenFileField.createWidget(url)).insertBefore(el.parentNode)[0];
      widget.parentNode.className += ' widen-filefield-parent';
    }
    return widget;
  };
  widenFileField.createWidget = function (url) {
    var $link = $('<a class="widen-filefield-link openwiden">' + Drupal.t('Open Widen Window') + '</a>');
    $link.attr('href', url).click(widenFileField.eLinkClick); //remove
    return $('<div class="widen-filefield-widget"></div>').append($link)[0];
  };

  widenFileField.eLinkClick = function (e) {
    e.preventDefault();
  };

})(jQuery, Drupal);
