(function($) {
  Drupal.behaviors.widen_display = {
    attach: function (context, settings) {
      $( "img[src*='widen.net']" ).each(function( index ) {
        var src_str = $(this).attr('src');
        if(src_str.indexOf('/sites/default/files') != -1){
            var arr = src_str.split('https/')[1];
            var string = arr.split('?')[0];
            var set_src = 'https://' + string;
            $(this).attr("src",set_src);
        }
      });
    }
  };
})(jQuery);
