(function ($, Drupal) {
  var selector = "";
  $(document).ready(function() {
    $(".openwiden").once("widen_interface").on('click', openModal);
    imagePreview();
    $(".widen-modal-close").click(closeModal);
    $('a.widen-filefield-link').click(function(event) {
      event.preventDefault();
      $('body').addClass('widenlink-clicked');
    });
    $(window).click(function(event) {
      if ($("#widenModal").is(":visible") && event.target == $("#widenModal")) {
        closeModal();
      }
    });
    window.addEventListener('message', function(event) {
      if ($("body").hasClass("widenlink-clicked")) {
      if(event.data.items && Array.isArray(event.data.items)) {
        var embedLink = event.data.items[0].embed_link;
        var thumbnailLink = event.data.items[0].thumbnail_link;
        if(selector && embedLink) {
          var assetURL = embedLink.split('?')[0];
          var assetThumbnailURL = assetURL;
          if(thumbnailLink) {
            assetThumbnailURL = thumbnailLink.split('?')[0];
          }
          $('[data-drupal-selector="' + selector + '-widen-paths"]').val(assetURL);    
          $('[data-drupal-selector="' + selector + '-upload-button"]').mousedown();  
          $('[data-drupal-selector="' + selector + '-preview"]').attr('src', assetThumbnailURL);
        }
        closeModal();
      }
      }
    });
  });
  $(document).ajaxComplete(function() {    
    $(".openwiden").once("widen_interface").on('click', openModal);
    $('a.widen-filefield-link').click(function(event) {
      event.preventDefault();
      $('body').addClass('widenlink-clicked');
    });
    imagePreview();
  });

  function imagePreview() {
    $(".image-widget").each(function( index ) {
      var widen_URL = $(this).find('.image-widget-data .file--image a').attr("href");
      if(widen_URL){
        if(widen_URL.indexOf('widen.net') > 0) {
          $(this).find('.image-preview img').attr('src', widen_URL);
        }
      }
    });
  }
  function closeModal() {
    $('#widenModal .widen-content').empty();
    $("body").removeClass('cke_dialog_open');
    $("body").removeClass('widenlink-clicked');
    $("#widenModal").hide();
  }
  function openModal() {
    var drupalSelector = $(this).parents('.widen-filefield-parent').find('input').first().data("drupal-selector");
    if(drupalSelector) {
      selector = drupalSelector.split('-upload')[0];
      $.get(Drupal.url('admin/widen/search_url'), function(data) {
        if (data.status_code == '200') {
          $('#widenModal .widen-content').empty();
          $('<iframe>', {
            src: data.url,
            id:  'widenSearchImg',
            frameborder: 0,
            scrolling: 'no',
            width: "100%",
            height: "100%"
            }).appendTo('#widenModal .widen-content');
          $("body").addClass('cke_dialog_open');
          $("#widenModal").show();
        }
        else {
          var msg = Drupal.t('Cannot connect to widen collective server. Please try again.');
          if (data.error) {
            msg = msg + '\n' + data.error;
          }
          alert(msg);
        }
      });
    }
  }
})(jQuery, Drupal);
